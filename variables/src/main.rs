struct Bleeh {
    name: String
}

impl Bleeh {
    fn bah(&self) -> usize {
        self.name.len()
    }
}

enum Foo {
    Ok,
    NotOk
}

fn thingy(fo: Foo) {
    println!("Hello world");

    match fo {
        Foo::Ok => println!("Ok"),
        _ => ()
    }
}

fn main() {
    let s = String::from("Helloo");

    let s1 = &s[0..2];

    thingy(Foo::Ok);
}
