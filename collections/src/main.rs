fn main() {
    let mut v = vec![1, 2, 3, 4, 2, 7, 4, 8];
    v.sort();

    println!("{:?}", v);

    let mut sum = 0;
    for i in &v {
        sum += i;
    }
    let mean = sum as f32 / v.len() as f32;
    println!("Mean: {}", mean);

    // Get middle number
    let median = if v.len() % 2 == 0 {
        // odd
        (v[v.len() / 2] + v[v.len() / 2 - 1]) as f32 / 2.
    } else {
        v[v.len() / 2] as f32
    };

    println!("Median: {}", median);


}
